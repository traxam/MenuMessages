/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.menumessages.spigot;

import de.tr808axm.menumessages.MenuMessagesStorage;
import de.tr808axm.menumessages.Reloadable;
import de.tr808axm.menumessages.spigot.command.MenuMessagesCommandExecutor;
import de.tr808axm.menumessages.spigot.listener.ServerListPingListener;
import de.tr808axm.spigot.pluginutils.ChatUtil;
import de.tr808axm.spigot.pluginutils.ConfigurationLoader;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * MenuMessages-Spigot-plugin's main class.
 * Created by tr808axm on 07.10.2016.
 */
public class MenuMessagesPlugin extends JavaPlugin implements ConfigurationLoader.ConfigurationReader, Reloadable {
    private ChatUtil chatUtil;
    private ConfigurationLoader messagesConfigurationLoader;
    private MenuMessagesStorage messagesStorage;
    private String[] firstLineMessages;
    private String[] secondLineMessages;

    public void onLoad() {
        super.onLoad();
        chatUtil = new ChatUtil(getDescription(), ChatColor.WHITE, getServer());
        try {
            ConfigurationLoader loader = new ConfigurationLoader(new File(getDataFolder(), "messages.yml"), getLogger(), new ConfigurationLoader.InputStreamProvider() {
                @Override
                public InputStream provideInputStream() {
                    return getResource("messages.yml");
                }
            });
            loader.addConfigurationReader(this);
        } catch (IOException e) {
            getLogger().severe("Error at loading/creating messages.yml: " + e.getClass().getSimpleName() + ": " + e.getMessage());
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
        messagesStorage = new MenuMessagesStorage(this);
        getLogger().info("Loaded!");
    }

    public void onEnable() {
        super.onEnable();
        registerCommands();
        registerEvents();
        getLogger().info("Enabled!");
    }

    public void onDisable() {
        super.onDisable();
        getLogger().info("Disabled!");
    }

    private void registerCommands() {
        getCommand("menumessages").setExecutor(new MenuMessagesCommandExecutor(this));
    }

    private void registerEvents() {
        getServer().getPluginManager().registerEvents(new ServerListPingListener(this), this);
    }

    public ChatUtil getChatUtil() {
        return chatUtil;
    }

    public MenuMessagesStorage getMessagesStorage() {
        return messagesStorage;
    }

    @Override
    public void readConfig(FileConfiguration fileConfiguration) {
        getLogger().info("Reading messages from disk...");
        ConfigurationSection messagesConfigurationSection = fileConfiguration.getConfigurationSection("messages");
        if (messagesConfigurationSection == null)
            messagesConfigurationSection = fileConfiguration.createSection("messages");
        List<String> firstLineStringList = messagesConfigurationSection.getStringList("first-line");
        if (firstLineStringList != null && !firstLineStringList.isEmpty()) {
            firstLineMessages = new String[firstLineStringList.size()];
            for (int i = 0; i < firstLineStringList.size(); i++)
                firstLineMessages[i] = ChatColor.translateAlternateColorCodes('&', firstLineStringList.get(i));
        } else {
            firstLineMessages = new String[]{getServer().getMotd()};
            getLogger().warning("String-List messages.yml/messages/first-line is empty, using server default MOTD as first-line-MOTD!");
        }
        List<String> secondLineStringList = messagesConfigurationSection.getStringList("second-line");
        if (secondLineStringList != null && !secondLineStringList.isEmpty()) {
            secondLineMessages = new String[secondLineStringList.size()];
            for (int i = 0; i < secondLineStringList.size(); i++)
                secondLineMessages[i] = ChatColor.translateAlternateColorCodes('&', secondLineStringList.get(i));
        } else {
            secondLineMessages = new String[]{""};
            getLogger().warning("String-List messages.yml/messages/second-line is empty, using empty String as second-line-MOTD!");
        }
    }

    @Override
    public MenuMessagesStorage.MessagesResult reload() {
        messagesConfigurationLoader.reload(true);
        return new MenuMessagesStorage.MessagesResult(firstLineMessages, secondLineMessages);
    }
}
