/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.menumessages.spigot.command;

import de.tr808axm.menumessages.spigot.MenuMessagesPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * Handles the /menumessages command.
 * Created by tr808axm on 10.10.2016.
 */
public class MenuMessagesCommandExecutor implements CommandExecutor {
    private final MenuMessagesPlugin plugin;

    public MenuMessagesCommandExecutor(MenuMessagesPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length >= 1 && (args[0].equalsIgnoreCase("reload") || args[0].equalsIgnoreCase("reload"))) {
            try {
                plugin.getMessagesStorage().reload();
                plugin.getChatUtil().send(sender, "Reload was successful.", true);
            } catch (NullPointerException e) {
                plugin.getChatUtil().send(sender, "Reload failed.", false);
            }
            return true;
        }
        plugin.getChatUtil().send(sender, plugin.getChatUtil().buildAboutMenu(), true);
        return true;
    }
}
