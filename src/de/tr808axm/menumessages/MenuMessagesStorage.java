/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.menumessages;

import java.util.Random;

/**
 * Used to store and randomize MOTDs.
 * Created by tr808axm on 10.10.2016.
 */
public class MenuMessagesStorage {
    public static class MessagesResult {
        final String[] firstLineMessages;
        final String[] secondLineMessages;

        public MessagesResult(String[] firstLineMessages, String[] secondLineMessages) {
            this.firstLineMessages = firstLineMessages;
            this.secondLineMessages = secondLineMessages;
        }
    }

    private String[] firstLineMessages;
    private String[] secondLineMessages;
    private Reloadable<MessagesResult> configLoader;

    public MenuMessagesStorage(Reloadable<MessagesResult> configLoader) {
        if (configLoader == null) throw new NullPointerException("configLoader may not be null");
        this.configLoader = configLoader;
        reload();
    }

    public String getRandomMessage() {
        Random random = new Random();
        return firstLineMessages[random.nextInt(firstLineMessages.length)]
                + "\n" + secondLineMessages[random.nextInt(secondLineMessages.length)];
    }

    public void reload() throws NullPointerException {
        MessagesResult result = configLoader.reload();
        if (result.firstLineMessages == null || result.firstLineMessages.length == 0)
            throw new NullPointerException("firstLineMessage may not be null or empty");
        if (result.secondLineMessages == null || result.secondLineMessages.length == 0)
            throw new NullPointerException("secondLineMessages may not be null or empty");
        this.firstLineMessages = result.firstLineMessages;
        this.secondLineMessages = result.secondLineMessages;
    }
}
