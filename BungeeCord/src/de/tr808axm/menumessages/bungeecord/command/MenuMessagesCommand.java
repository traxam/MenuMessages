/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.menumessages.bungeecord.command;

import de.tr808axm.menumessages.MenuMessagesStorage;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

/**
 * Handles and responds to the /menumessages command.
 * Created by tr808axm on 23.11.2016.
 */
public class MenuMessagesCommand extends Command {
    private final MenuMessagesStorage storage;

    public MenuMessagesCommand(MenuMessagesStorage storage) {
        super("menumessages");
        this.storage = storage;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length >= 1 && (args[0].equalsIgnoreCase("reload") || args[0].equalsIgnoreCase("reload"))) {
            try {
                storage.reload();
                sender.sendMessage(new TextComponent(ChatColor.GREEN + "Reload was successful."));
            } catch (NullPointerException e) {
                sender.sendMessage(new TextComponent(ChatColor.RED + "Reload failed."));
            }
            return;
        }
        TextComponent link = new TextComponent("https://gitlab.com/tr808axm/MenuMessages");
        link.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, link.getText()));
        sender.sendMessage(
                new TextComponent("MenuMessages is a free BungeeCord/Spigot-plugin by tr808axm. To get it, visit "),
                link, new TextComponent("."));
    }
}
