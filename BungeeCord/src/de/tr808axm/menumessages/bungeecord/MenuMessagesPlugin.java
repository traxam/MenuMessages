/*
 * Copyright (C) 2016  tr808axm
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.tr808axm.menumessages.bungeecord;

import de.tr808axm.menumessages.MenuMessagesStorage;
import de.tr808axm.menumessages.Reloadable;
import de.tr808axm.menumessages.bungeecord.command.MenuMessagesCommand;
import de.tr808axm.menumessages.bungeecord.listener.ProxyPingListener;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.List;

/**
 * MenuMessages-BungeeCord-plugin's main class.
 * Created by tr808axm on 22.11.2016.
 */
public class MenuMessagesPlugin extends Plugin implements Reloadable<MenuMessagesStorage.MessagesResult> {
    private MenuMessagesStorage messagesStorage;
    private File configFile;

    public void onLoad() {
        super.onLoad();
        configFile = new File(getDataFolder(), "messages.yml");
        if (!configFile.exists()) {
            try {
                getLogger().info("Configuration file not found, creating...");
                configFile.getParentFile().mkdirs();
                InputStream defaultConfig = getResourceAsStream("messages.yml");
                Files.copy(defaultConfig, configFile.toPath());
            } catch (IOException e) {
                getLogger().severe("FATAL: Could not create configuration: " + e.getClass().getSimpleName() + ": " + e.getMessage());
                return;
            }
        }

        messagesStorage = new MenuMessagesStorage(this);
        getLogger().info("Loaded!");
    }

    public void onEnable() {
        super.onEnable();
        registerCommands();
        registerEvents();
        getLogger().info("Enabled!");
    }

    private void registerCommands() {
        getProxy().getPluginManager().registerCommand(this, new MenuMessagesCommand(messagesStorage));
    }

    public void onDisable() {
        super.onDisable();
        getLogger().info("Disabled!");
    }

    private void registerEvents() {
        getProxy().getPluginManager().registerListener(this, new ProxyPingListener(this));
    }

    public MenuMessagesStorage getMessagesStorage() {
        return messagesStorage;
    }

    @Override
    public MenuMessagesStorage.MessagesResult reload() {
        String[] firstLineMessages = null;
        String[] secondLineMessages = null;
        try {
            Configuration configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
            Configuration messagesConfigurationSection = configuration.getSection("messages");
//            if(messagesConfigurationSection == null) TODO
//                messagesConfigurationSection = configuration.createSection("messages");
            List<String> firstLineStringList = messagesConfigurationSection.getStringList("first-line");
            if (firstLineStringList != null && !firstLineStringList.isEmpty()) {
                firstLineMessages = new String[firstLineStringList.size()];
                for (int i = 0; i < firstLineStringList.size(); i++)
                    firstLineMessages[i] = ChatColor.translateAlternateColorCodes('&', firstLineStringList.get(i));
            } else {
                firstLineMessages = new String[]{getProxy().getName()};
                getLogger().warning("String-List messages.yml/messages/first-line is empty, using server name as first-line-MOTD!");
            }
            List<String> secondLineStringList = messagesConfigurationSection.getStringList("second-line");
            if (secondLineStringList != null && !secondLineStringList.isEmpty()) {
                secondLineMessages = new String[secondLineStringList.size()];
                for (int i = 0; i < secondLineStringList.size(); i++)
                    secondLineMessages[i] = ChatColor.translateAlternateColorCodes('&', secondLineStringList.get(i));
            } else {
                secondLineMessages = new String[]{""};
                getLogger().warning("String-List messages.yml/messages/second-line is empty, using empty String as second-line-MOTD!");
            }
        } catch (IOException e) {
            getLogger().severe("FATAL: Could not load configuration: " + e.getClass().getSimpleName() + ": " + e.getMessage());
        }
        return new MenuMessagesStorage.MessagesResult(firstLineMessages, secondLineMessages);
    }
}
